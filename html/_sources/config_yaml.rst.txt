config.yml
===========

At the root of every job that uses the pipeline should be a file called
:code:`config.yml`. This file is written in `yaml
<https://camel.readthedocs.io/en/latest/yamlref.html>`_ and describes where the
project files reside in the folder structure as well as configurations for the
various plugins. We'll step through each of the top level entries in the file
one by one and a complete example will be at the bottom.

.. note::
   When creating a new job, this file will often be called config_disabled.yml.
   This is a way to make sure jobs that don't need to be acivated in the
   pipeline are left out. To activate it, just rename it :code:`config.yml`.


ver
---
.. code-block:: yaml

   ver: 1.5

We keep track of the current version of the configuration in case  
non backwards compatible changes are made. So far no breaking changes have been
made so it's mostly a reference.

globals
-------
.. code-block:: yaml

   globals:
     $to_spot: <job_path>/Design/Production/<spot>
     $to_shot: <job_path>/Design/Production/<spot>/Shots/<shot>
     $to_asset: <job_path>/Design/Production/<spot>/CG_Assets/<category>/<asset>

Variables stored under globals are handy shortcuts that can be used anywhere
else in the config. Starting global variables with a $ is just suggested to
indicate it's a global variable, but isn't required. 

software
--------
.. code-block:: yaml

   software:
     default:
        launcher_profiles:
          shots: <$to_shot>/Projects/CG/<step>
          previz: <job_path>/Design/Production/<spot>/CG_Previz/<previz_shot>/Projects/CG/<step>
          RnD: <job_path>/Design/Production/<spot>/CG_RnD/<rnd_shot>/Projects/CG/<step>
  
     nuke:
        launcher_profiles:
          shots: <$to_shot>/Projects/Nuke
          livegroups: <$to_shot>/Projects/Nuke/Livegroups/<live_group>
          previz: <job_path>/Design/Production/<spot>/CG_Previz/<previz_shot>/Projects/Nuke
          assets: <job_path>/Design/Production/<spot>/CG_Assets/<asset>
          RnD: <job_path>/Design/Production/<spot>/CG_RnD/<rnd_shot>/Projects/Nuke
          common: <job_path>/Design/Production/<spot>/Shots/_common_/Projects/Nuke
   ...

This is the bulk of the config file. For each software there's a section 
describing where each of the supported profiles reside. There's also a :code:`default`
section. When the pipeline is looking for launcher profiles that a software supports, it 
will first grab all the default ones. If that software defines it's own profile 
of the same name, it will get overwritten. For example, in the config above, 
shots is defined by default. Nuke however overrides that with its own path. 

This is also where some software specific tool configurations are stored.

exclude
-------
.. code-block:: yaml

   exclude:
     asset:
       - Common
       # - _common_
     spot:
       -archive
     shot:
       - Playground
       - Conform
       - Editorial
       - Common
       - _common_
     step:
       - renders
       - shaders
       - textures
     category:
       - _common_

Sometimes there are folders that should be ignored when selecting a token. For 
example, where you define your :code:`<shot>` token, there may be folders that 
aren't actually shots. Maybe an archive folder for when a shot shouldn't be 
used. Here's where you would tell the pipeline not to list it. Add an element 
with the name of the token and add the folder you'd like to exclude to the list 
denoted with a dash.

template_directory
------------------
.. code-block:: yaml

   template_directory: <job_path>/Utilities/Empty_Files

This simply describes where the folder that holds the empy project files for 
each software is. Change it if the folder structure changes.